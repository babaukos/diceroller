﻿/*
 *
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManeger : MonoBehaviour 
{
    public Controller contr;
	public LayerMask collisionLayer;
	
	public bool creat;
    public List<Dice> Manager;
	private Transform spawn;
	private Camera camera;	
	
	// Use this for initialization
	void Start () 
	{
		spawn = transform.Find("SpawnPoint");	
        camera = Camera.main;		
	}
	
	// Update is called once per frame
	void Update () 
	{
		  Dice d;
		  Ray ray;
		  RaycastHit hit;
          ray = camera.ScreenPointToRay(Input.mousePosition);
		  if (Input.GetButtonDown ("Fire1"))
          {
          if (Physics.Raycast(ray, out hit, collisionLayer)) 
		  {
			  d = hit.transform.GetComponent<Dice>();
			  if(!creat)
		      {
			     if(d != null)
			     {
                    d.DeletDice();
			     }
			  } 
			   else
			       {
					 if(d != null)
			         {
                       d.DropDice();
			         } 
				   }
		  }}
	}

	//
	public void Mode()
	{
		creat = !creat;
	}
	//
	public void AddDice(string n)
	{
		creat = true;
		GameObject D;
		D = (GameObject)Instantiate(Manager[int.Parse(n)].gameObject, spawn.position, Quaternion.identity);
		contr.dices.Add(D.GetComponent<Dice>());
	}
	//
    public void DeletDice()
    {
		//contr.dices.Add();
	}
}
