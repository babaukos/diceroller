﻿using UnityEngine;
using System.Collections;

public class NextScen : MonoBehaviour 
{
    public float timeChange;
    public string nextScene;
	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (timeChange>0)
        {
            timeChange -= 1 * Time.deltaTime;
        }
        else
            {
                //ApplicationSceneManager.LoadScene(xxx, LoadSceneMode.Additive);
                Application.LoadLevel(nextScene);
            }

	}
}
