﻿/*
 *
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour 
{
	public bool recalculate;
    public int summeryValue;
	public int qurentValue;
	
    public List<Dice> dices;

	// Use this for initialization
	void Start () 
	{

	}
	// Сброс сумми
	public void Reset()
	{
      summeryValue = 0;
	  // qurentValue = 0;
	  // recalculate = true;		
	}
	// Update is called once per frame
	void Update ()
	{
		foreach(Dice d in dices)
		{
			if((d.speed == 0))
			{

				if(recalculate)
				{
			      summeryValue += d.diceValue;
			      qurentValue = d.diceValue;
				  recalculate = false;
				}
			}
			 else{recalculate = true;}
		}
	}
	// Если встряхиваем устройство
	public void ShakeField()
	{
		foreach(Dice d in dices)
		{
			d.DropDice();
		}
	}
}
