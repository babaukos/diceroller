﻿/*
 *
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour 
{
	public int diceValue;

	public float forceAmount = 10.0f;
    public float torqueAmount = 10.0f;
	
	public ForceMode forceMode;
	public LayerMask collisionLayer;

	public Ray[] side;
	
	private Rigidbody RiBody;  // компонент
	public float speed = 0.0f; // скорость кости
	public bool recalc = true; // считать и пересчитать
	
	[System.Serializable]
	public struct Ray
	{
	  public Transform sideVector;	
	  public int sideValue;
	}
	// Use this for initialization
	void Start () 
	{
	   RiBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		speed = RiBody.velocity.magnitude;
		if(speed > 0)
		 recalc = true;
	}
	// Подкидываем кубик
	public void DropDice()
	{
		RiBody.AddForce(Random.onUnitSphere * forceAmount, forceMode);
        RiBody.AddTorque(Random.onUnitSphere * torqueAmount, forceMode);
	}
	public void DeletDice()
	{
		Destroy(gameObject);
	}
    // Расчет луча для получения значения
	void FixedUpdate()
	{
		for(int r = 0; r < side.Length; r++)
		{
			if(speed == 0)
		    {
				if(recalc)
				{
			       if (Physics.Linecast(transform.position, side[r].sideVector.position, collisionLayer))
			       {
					  recalc = false;
				      Debug.Log(r + "|" + side[r].sideValue);
                      diceValue = side[r].sideValue;
			       }
				}
	        }
		}	
	}
	
	public void OnDrawGizmos()
	{
		for(int r = 0; r < side.Length; r++)
		{
			Debug.DrawLine(transform.position, side[r].sideVector.position, Color.green);
		}
	}
}
